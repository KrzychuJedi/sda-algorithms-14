package exercises;

/**
 * Za pomocą rekurencji rozdziel podany ciąg znaków od
 * użytkownika gwiazdką (*).
 * <p>
 * Wejście:
 * Marek
 * Wyjście
 * M*a*r*e*k
 *
 * @author marek.sobieraj on 2018-03-08
 */
public class Exercise12 {

    public static void main(String[] args) {
        System.out.println(starText("example"));
    }

    public static String starText(String text) {
        // TODO:
        return text;
    }
}
