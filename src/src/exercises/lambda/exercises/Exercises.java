package exercises.lambda.exercises;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * @author marek.sobieraj on 2018-03-16
 * <p>
 * Zadanie:
 * Wykonaj kolejne polecenia wykorzystując strumienie.
 */
public class Exercises {

    private static List<Water> waters = Arrays.asList(
            new Water("Nałęczowianka", BigDecimal.valueOf(3.49), true, 0.5),
            new Water("Nałęczowianka", BigDecimal.valueOf(2.99), false, 0.6),
            new Water("Staropolanka", BigDecimal.valueOf(1.99), true, 1),
            new Water("Staropolanka", BigDecimal.valueOf(2.49), false, 1),
            new Water("Primavera", BigDecimal.valueOf(0.99), true, 0.33),
            new Water("Primavera", BigDecimal.valueOf(0.89), false, 0.33),
            new Water("Muszynianka", BigDecimal.valueOf(3.89), true, 1.5),
            new Water("Muszynianka", BigDecimal.valueOf(3.49), false, 1.5)
    );

    public static void main(String[] args) {

//        1. Wypisz wszystkie wody


//        2. Wypisz wszystkie wody gazowane


//        3. Wypisz wszystkie rodzaje wód (bez duplikatów), tylko same nazwe


//        4. Posortuj wody wg ceny


//        5. Wypisz najtańszą wodę


//        6. Wypisz najdroższą wodę


//        7. Podaj średnią wielkość butelki wody (size of Water)


//        8. Wypisz kwotę do zapłaty za wszystkie wody po jednej sztuce (suma)


//        9. Wypisz średni koszt wody (pierw wykonaj zadanie powyższe)


//        10. Wypisz najdroższą wodę wśród wód gazowanych


//        11. Wypisz najtańszą wodę wśród wód niegazowanych


//        12. Wypisz wszystkie wody (bez duplikatów), których nazwa kończy się na '-anka'


//        13. Pogrupuj wody po nazwie, wypisując ich ceny posortowane rosnąca po przecinku, np. Muszynianka 3,49, 3.89


//        14. Rozszerz zadanie 13 tak, aby wody były dodatkowo posortowane alfabetycznie


//        15. Wypisz 3 nazwy wód, których rozmiar butelki jest największy
    }
}
