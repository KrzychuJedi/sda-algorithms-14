package exercises.lambda.introduction;


import exercises.lambda.introduction.materials.DateRange;
import exercises.lambda.introduction.materials.Robot;
import exercises.lambda.introduction.materials.RobotGroup;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

/**
 * @author marek.sobieraj on 2018-03-15
 * <p>
 * Lambdy są dostępne w języku Java od wersji 1.8.
 * Poniżej znajdziecie kilka przykładów użycia lambd.
 */
public class ExampleLambdaUsage {

    public static void main(String[] args) {
//        example 1 - shortcut (woman group)
        List<Robot> robotGroup = RobotGroup.getRobotGroup();
        RobotGroup.print(robotGroup, (robot) -> robot.isDancer());
/*        RobotGroup.print(womenGroup, Robot::isDancer);*/


//        example  2 - not compilation declaration
/*        List<String> names = Arrays.asList("Iza", "Ewa", "Kasia", "Agnieszka");
        names.forEach((name) -> {
                    int name2 = 2;
                    System.out.println("Imię: " + name);
                }
        );
        names.forEach(System.out::println);*/

//        example 3 - primitive types as streams
        int[] values = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};

//        IntStream.of(values).filter(number -> number > 5).forEach(System.out::println);
//

//        example 4 - what do reduce() ?
//        IntStream.of(values).reduce((result, current) -> {
//            return result + current;
//        });

        LocalDate now = LocalDate.now();
        LocalDate fiveDaysLater = now.plusDays(5);
        LocalDate tenDaysLater = now.plusDays(10);
        List<LocalDate> events = Arrays.asList(now, fiveDaysLater, tenDaysLater);

        List<DateRange> ranges = new ArrayList<>();
        events.stream().reduce((next, current) -> {
            ranges.add(new DateRange(next, current));
            return current;
        });

        ranges.forEach(System.out::println);

//         example 4 - map + filter + print
/*        List<String> fruits = Arrays.asList("apple", "banana", "cherry", "plum", "pear", "pinapple");
        List<String> filtered = new ArrayList<>();
        for (String fruit : fruits) {
            if (fruit.startsWith("p")) {
                filtered.add(fruit.toUpperCase());
            }
        }
        filtered.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });
        for (String fruit : filtered) {
            System.out.println(fruit);
        }*/

/*        fruits.stream()
                .filter(s -> s.startsWith("p"))
                .map(String::toUpperCase)
                .sorted()
                .forEach(System.out::println);*/

//        Example 5 - simple parallel
        /*IntStream.range(1, 10).parallel().forEach(System.out::println);*/
//        END
    }
}
