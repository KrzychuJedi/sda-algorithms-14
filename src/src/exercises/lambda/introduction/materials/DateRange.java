package exercises.lambda.introduction.materials;

import java.time.LocalDate;

public class DateRange {

    private LocalDate start;

    private LocalDate end;

    public DateRange(LocalDate start, LocalDate end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public String toString() {
        return "[" + start + " - " + end + "]";
    }
}
