package together;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Scanner;

public class Exercise5 {

    public static void main(String[] args) {
        BigDecimal[] data = new BigDecimal[]{new BigDecimal(200), new BigDecimal(100), new BigDecimal(50),
                new BigDecimal(20), new BigDecimal(10), new BigDecimal(5), new BigDecimal(2), new BigDecimal(1),
                new BigDecimal("0.50"), new BigDecimal("0.20"), new BigDecimal("0.10"), new BigDecimal("0.05"), new BigDecimal("0.02"), new BigDecimal("0.01")};

        System.out.println("Podaj kwotę:");
        Scanner s = new Scanner(System.in);
        BigDecimal kwota = s.nextBigDecimal();

        for (int i = 0; i < data.length; i++) {
            if (kwota.compareTo(data[i]) >= 0) {
                BigDecimal times = kwota.divide(data[i], 0, RoundingMode.FLOOR);
                BigDecimal value = times.multiply(data[i]);
                kwota = kwota.subtract(value);
                System.out.println(times + " x " + data[i]);
            }
        }
    }
}
